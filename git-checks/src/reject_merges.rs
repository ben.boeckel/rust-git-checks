// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;
use git_checks_core::impl_prelude::*;

/// A check which denies merge commits, including octopus merges.
#[derive(Builder, Debug, Default, Clone, Copy)]
#[builder(field(private))]
pub struct RejectMerges {}

impl RejectMerges {
    /// Create a new builder.
    pub fn builder() -> RejectMergesBuilder {
        RejectMergesBuilder::default()
    }
}

impl Check for RejectMerges {
    fn name(&self) -> &str {
        "reject-merges"
    }

    fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        if commit.parents.len() > 1 {
            result.add_error(format!(
                "commit {} not allowed; it is a merge commit.",
                commit.sha1,
            ));
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use git_checks_config::{register_checks, CommitCheckConfig, IntoCheck};
    use serde::Deserialize;
    #[cfg(test)]
    use serde_json::json;

    use crate::RejectMerges;

    /// Configuration for the `RejectMerges` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"reject_merges"` and a topic
    /// check with the name `"reject_merges/topic"`.
    #[derive(Deserialize, Debug)]
    pub struct RejectMergesConfig {}

    impl IntoCheck for RejectMergesConfig {
        type Check = RejectMerges;

        fn into_check(self) -> Self::Check {
            RejectMerges::default()
        }
    }

    register_checks! {
        RejectMergesConfig {
            "reject_merges" => CommitCheckConfig,
        },
    }

    #[test]
    fn test_reject_merges_config_empty() {
        let json = json!({});
        let check: RejectMergesConfig = serde_json::from_value(json).unwrap();

        let _ = check.into_check();
    }
}

#[cfg(test)]
mod tests {
    use git_checks_core::Check;

    use crate::test::*;
    use crate::RejectMerges;

    const NO_MERGES_TOPIC: &str = "2fab950c4ace0584760e00caae1bb7913b07494e";
    const WITH_MERGES_TOPIC: &str = "92f545fa6c9326fe11dfb69c3ee01af285b595f4";
    const OCTOPUS_MERGES_TOPIC: &str = "4447a8eddbccac61daa6b55642e46156011d36cb";

    #[test]
    fn test_reject_merges_builder_default() {
        assert!(RejectMerges::builder().build().is_ok());
    }

    #[test]
    fn test_reject_merges_name_commit() {
        let check = RejectMerges::default();
        assert_eq!(Check::name(&check), "reject-merges");
    }

    #[test]
    fn test_reject_merges_name_topic() {
        let check = RejectMerges::default();
        assert_eq!(Check::name(&check), "reject-merges");
    }

    #[test]
    fn test_reject_merges_no_merges() {
        let check = RejectMerges::default();
        run_check_ok("test_reject_merges_no_merges", NO_MERGES_TOPIC, check);
    }

    #[test]
    fn test_reject_merges_with_merges() {
        let check = RejectMerges::default();
        let result = run_check("test_reject_merges_with_merges", WITH_MERGES_TOPIC, check);
        test_result_errors(result, &[
            "commit 92f545fa6c9326fe11dfb69c3ee01af285b595f4 not allowed; it is a merge commit.",
        ]);
    }

    #[test]
    fn test_reject_merges_octopus_merges() {
        let check = RejectMerges::default();
        let result = run_check("test_reject_merges_no_merges", OCTOPUS_MERGES_TOPIC, check);
        test_result_errors(result, &[
            "commit 4447a8eddbccac61daa6b55642e46156011d36cb not allowed; it is a merge commit.",
        ]);
    }
}
