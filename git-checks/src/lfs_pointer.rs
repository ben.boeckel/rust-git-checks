// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;
use git_checks_core::impl_prelude::*;
use lazy_static::lazy_static;
use regex::Regex;

/// A check which verifies that files meant to be under LFS control are valid LFS pointers.
///
/// This is based on the `filter` attribute for a file being set to `lfs`.
#[derive(Builder, Debug, Default, Clone, Copy)]
#[builder(field(private))]
pub struct LfsPointer {}

impl LfsPointer {
    /// Create a new builder.
    pub fn builder() -> LfsPointerBuilder {
        LfsPointerBuilder::default()
    }

    fn check_line(
        prev_key: Option<String>,
        key: &str,
        value: &str,
        content: &dyn Content,
        name: &str,
    ) -> CheckResult {
        let mut result = CheckResult::new();

        if let Some(old_key) = prev_key {
            if old_key == "version" {
                // Ignore; the first non-version key is always fine.
            } else if key < old_key.as_str() {
                result.add_error(format!(
                    "{}unsorted key `{}` found in LFS pointer `{}`.",
                    commit_prefix_str(content, "not allowed;"),
                    key,
                    name,
                ));
            }
        } else if key == "version" {
            if value != GIT_LFS_SPEC_URL {
                result.add_warning(format!(
                    "{}unexpected git-lfs version string in `{}`: `{}`.",
                    commit_prefix_str(content, "contains an"),
                    name,
                    value,
                ));
            }
        } else {
            result.add_error(format!(
                "{}the first key in LFS pointer `{}` must be `version`; found `{}`.",
                commit_prefix_str(content, "not allowed;"),
                name,
                key,
            ));
        }

        match key {
            "oid" => {
                let mut split = value.splitn(2, ':');
                let algo = split.next();
                let digest = split.next();

                const fn ascii_hex_len(bits: usize) -> usize {
                    // bits / 8 (bits per byte) * 2 (hex digits per byte)
                    bits >> 2
                }

                fn is_ascii_hex(digest: &str) -> bool {
                    digest
                        .bytes()
                        .all(|b| b.is_ascii_hexdigit() && !b.is_ascii_uppercase())
                }

                let mut check_algo_digest = |algo, digest: &str, bits| {
                    if digest.is_empty() {
                        result.add_error(format!(
                            "{}empty digest for {} algorithm in LFS pointer `{}`.",
                            commit_prefix_str(content, "not allowed;"),
                            algo,
                            name,
                        ));
                    } else if digest.len() != ascii_hex_len(bits) {
                        result.add_error(format!(
                            "{}invalid digest length in `{}` for {} algorithm in LFS pointer `{}`.",
                            commit_prefix_str(content, "not allowed;"),
                            digest,
                            algo,
                            name,
                        ));
                    } else if !is_ascii_hex(digest) {
                        result.add_error(format!(
                            "{}invalid digest content `{}` for {} algorithm in LFS pointer `{}`.",
                            commit_prefix_str(content, "not allowed;"),
                            digest,
                            algo,
                            name,
                        ));
                    }
                };

                match algo {
                    Some("") | None => {
                        result.add_error(format!(
                            "{}missing hash algorithm in LFS pointer `{}`.",
                            commit_prefix_str(content, "not allowed;"),
                            name,
                        ));
                    },
                    Some(algo) => {
                        if let Some(digest) = digest {
                            match algo {
                                algo @ "sha256" => check_algo_digest(algo, digest, 256),
                                algo => {
                                    result.add_warning(format!(
                                        "{}unrecognized hash algorithm `{}` in LFS pointer `{}`.",
                                        commit_prefix_str(content, "contains an"),
                                        algo,
                                        name,
                                    ));

                                    if digest.is_empty() {
                                        result.add_error(format!(
                                            "{}empty digest for {} algorithm in LFS pointer `{}`.",
                                            commit_prefix_str(content, "not allowed;"),
                                            algo,
                                            name,
                                        ));
                                    }
                                },
                            }
                        } else {
                            result.add_error(format!(
                                "{}missing digest for {} algorithm in LFS pointer `{}`.",
                                commit_prefix_str(content, "not allowed;"),
                                algo,
                                name,
                            ));
                        }
                    },
                }
            },
            "size" => {
                if let Ok(value) = value.parse::<u64>() {
                    if value == 0 {
                        result.add_error(format!(
                            "{}the `size` value in LFS pointer `{}` must be greater than 0; found \
                             `{}`.",
                            commit_prefix_str(content, "not allowed;"),
                            name,
                            value,
                        ));
                    }
                } else {
                    result.add_error(format!(
                        "{}the `size` key value in LFS pointer `{}` must be an unsigned integer; \
                         found `{}`.",
                        commit_prefix_str(content, "not allowed;"),
                        name,
                        value,
                    ));
                }
            },
            key => {
                fn is_valid_key(key: &str) -> bool {
                    key.bytes().all(|b| {
                        b.is_ascii_lowercase() || b.is_ascii_digit() || b == b'.' || b == b'-'
                    })
                }

                if !is_valid_key(key) {
                    // This is not actually reachable without changing the regular expression which
                    // only matches valid keys.
                    result.add_error(format!(
                        "{}the key `{}` is not valid in LFS pointer `{}`",
                        commit_prefix_str(content, "not allowed;"),
                        key,
                        name,
                    ));
                }
            },
        }

        result
    }
}

const GIT_LFS_SPEC_URL: &str = "https://git-lfs.github.com/spec/v1";

lazy_static! {
    static ref LFS_LINE_RE: Regex = Regex::new(
        "^(?P<key>[a-z0-9.-]*) \
         (?P<value>[^\r\n]*)$",
    )
    .unwrap();
}

impl ContentCheck for LfsPointer {
    fn name(&self) -> &str {
        "lfs-pointer"
    }

    fn check(
        &self,
        ctx: &CheckGitContext,
        content: &dyn Content,
    ) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            match diff.status {
                StatusChange::Added | StatusChange::Modified(_) => (),
                _ => continue,
            }

            let filter_attr = ctx.check_attr("filter", diff.name.as_path())?;
            if let AttributeState::Value(filter_name) = filter_attr {
                if filter_name != "lfs" {
                    continue;
                }
            } else {
                continue;
            }

            let cat_file = ctx
                .git()
                .arg("cat-file")
                .arg("blob")
                .arg(diff.new_blob.as_str())
                .output()
                .map_err(|err| GitError::subcommand("cat-file", err))?;
            let lfs_pointer = if let Ok(lfs_pointer) = String::from_utf8(cat_file.stdout) {
                lfs_pointer
            } else {
                result.add_error(format!(
                    "{}invalid utf-8 sequence in an LFS pointer added in `{}`.",
                    commit_prefix_str(content, "not allowed;"),
                    diff.name,
                ));
                continue;
            };

            // Empty files are valid LFS pointers (for empty files).
            if lfs_pointer.is_empty() {
                continue;
            }

            let (lfs_res, _, has_oid, has_size) = lfs_pointer.lines().enumerate().fold(
                (CheckResult::new(), None, false, false),
                |data, (count, line)| {
                    let (mut lfs_res, prev_key, has_oid, has_size) = data;

                    if let Some(lfs_line) = LFS_LINE_RE.captures(line) {
                        let key = lfs_line
                            .name("key")
                            .expect("the LFS regex should have a 'key' group");
                        let value = lfs_line
                            .name("value")
                            .expect("the LFS regex should have a 'value' group");

                        if Some(key.as_str()) == prev_key.as_deref() {
                            lfs_res.add_error(format!(
                                "{}duplicate key `{}` in an LFS pointer added in `{}`.",
                                commit_prefix_str(content, "not allowed;"),
                                key.as_str(),
                                diff.name,
                            ));
                        }

                        let line_res = Self::check_line(
                            prev_key,
                            key.as_str(),
                            value.as_str(),
                            content,
                            diff.name.as_str(),
                        );

                        let new_has_oid = has_oid || key.as_str() == "oid";
                        let new_has_size = has_size || key.as_str() == "size";

                        (
                            lfs_res.combine(line_res),
                            Some(key.as_str().into()),
                            new_has_oid,
                            new_has_size,
                        )
                    } else {
                        lfs_res.add_error(format!(
                            "{}invalid line in an LFS pointer added in `{}` on line {}.",
                            commit_prefix_str(content, "not allowed;"),
                            diff.name,
                            count + 1,
                        ));

                        (lfs_res, prev_key, has_oid, has_size)
                    }
                },
            );
            result = result.combine(lfs_res);

            if !has_oid {
                result.add_error(format!(
                    "{}an LFS pointer is missing the `oid` key in `{}`.",
                    commit_prefix_str(content, "not allowed;"),
                    diff.name,
                ));
            }

            if !has_size {
                result.add_error(format!(
                    "{}an LFS pointer is missing the `size` key in `{}`.",
                    commit_prefix_str(content, "not allowed;"),
                    diff.name,
                ));
            }
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use git_checks_config::{register_checks, CommitCheckConfig, IntoCheck, TopicCheckConfig};
    use serde::Deserialize;
    #[cfg(test)]
    use serde_json::json;

    use crate::LfsPointer;

    /// Configuration for the `LfsPointer` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"lfs_pointer"` and a topic check
    /// with the name `"lfs_pointer/topic"`.
    #[derive(Deserialize, Debug)]
    pub struct LfsPointerConfig {}

    impl IntoCheck for LfsPointerConfig {
        type Check = LfsPointer;

        fn into_check(self) -> Self::Check {
            LfsPointer::default()
        }
    }

    register_checks! {
        LfsPointerConfig {
            "lfs_pointer" => CommitCheckConfig,
            "lfs_pointer/topic" => TopicCheckConfig,
        },
    }

    #[test]
    fn test_lfs_pointer_config_empty() {
        let json = json!({});
        let check: LfsPointerConfig = serde_json::from_value(json).unwrap();

        let _ = check.into_check();
    }
}

#[cfg(test)]
mod tests {
    use git_checks_core::{Check, TopicCheck};

    use crate::test::*;
    use crate::LfsPointer;

    const LFS_INVALID_UTF8: &str = "c413d8954d903557de00be9d96b4daa264bd4b22";
    const LFS_INVALID_LINE_FORMAT: &str = "709c2cd38863717231453bb4945897a0ebef3f80";
    const LFS_MISSING_VERSION: &str = "0ec2ab0e229fdcbff18a8bfd55774e4f71b04bbb";
    const LFS_MISPLACED_VERSION: &str = "c86f7b78fd6ca471d97f3861addf3a35c25f3504";
    const LFS_INVALID_VERSION: &str = "f799c407f7678ba38f87720fb4217ece6c50f7e5";
    const LFS_DUPLICATE_KEY: &str = "4d0ead1cac518874d1f7cd005569238b7b9bc7c3";
    const LFS_MISSING_SIZE: &str = "e6f342338093141e365b1bb1596290c155198c60";
    const LFS_MISSING_OID: &str = "ceeabe41a44a4469b6644017af725b0127d92302";
    const LFS_UNRECOGNIZED_HASH: &str = "1aa2873adebe911caf594c376efc5c522f8d4c3a";
    const LFS_MISSING_HASH: &str = "109806f98fd036cba88d07f94df162aad5086d0b";
    const LFS_UNSORTED_KEYS: &str = "aaeedde2f188005c24e090bc44378b17770e3b94";
    const LFS_EMPTY_POINTER: &str = "186cb934ce4b20f85ee6bfb834ab5652121e6436";
    const LFS_VALID_POINTER: &str = "e088802e2abc6b7c2bb99e194bcc074a9cf9076c";
    const LFS_SHA256_BAD_DIGEST_LEN: &str = "b00442c09d00aeb1ffabffea821e8b286da6b0df";
    const LFS_SHA256_BAD_DIGEST_CONTENT: &str = "1cd551597d65f74a57928c024ff58edb532767e0";
    const LFS_SHA256_EMPTY_DIGEST: &str = "6d377c8197b372df64689cb90db70904a2667dc9";
    const LFS_UNKNOWN_ALGO_EMPTY_DIGEST: &str = "96c193edaf748309b7bb5c2e816abda4a06c80c0";
    const LFS_MISSING_ALGO: &str = "6b4125e802283d716ceef0f4c7630a94810654fd";
    const LFS_INVALID_KEY: &str = "335b860660b133c59c651b3cb22ac883fdb8d004";
    const LFS_EMPTY_KEY: &str = "7516f6f1e68245da397fd459e0516ca3f2334aa5";
    const LFS_DELETED_BAD_FILE: &str = "94c4f73c20e547ec9ea45b92a1b10b1b4e7d0505";
    const LFS_NONINT_SIZE: &str = "3ed812f83b9bd5d8ed5538933254d07a42928e47";
    const LFS_ZERO_SIZE: &str = "5523b49074a66611aaafcac32cf7d05a654f552c";
    const LFS_NOT_LFS_FILTER: &str = "be247837cbdbcbbb8a7879c638c918e59bc61e56";

    const LFS_INVALID_UTF8_FIXED: &str = "5997c958735f9d4eab0640fa54b4c094bd74e71b";
    const LFS_INVALID_LINE_FORMAT_FIXED: &str = "501a70c9ff37a7ce9e7c853a737b93e3be6d73d1";
    const LFS_MISSING_VERSION_FIXED: &str = "225108803b9a8679c48272dc83765ce5a6a281e1";
    const LFS_MISPLACED_VERSION_FIXED: &str = "7f2c99eab3e1cb58e41bdfe0ae56dfa5c6529a26";
    const LFS_INVALID_VERSION_FIXED: &str = "ce3f57bc200ebe52cce3b10580ce6adc76e830bd";
    const LFS_DUPLICATE_KEY_FIXED: &str = "59dafc938b6fbf913624afd16b3732c79f88e4b5";
    const LFS_MISSING_SIZE_FIXED: &str = "9f8196108b33fc91884be01b63dd82bb484b41be";
    const LFS_MISSING_OID_FIXED: &str = "b88474b22be8e87d2276fb9e54bf30147638d9b2";
    const LFS_UNRECOGNIZED_HASH_FIXED: &str = "68aaf7516f3f22705a87ad19799bd03a9c5ae999";
    const LFS_MISSING_HASH_FIXED: &str = "31d34a4fb619de4e4877e426255bd206b4f579b9";
    const LFS_UNSORTED_KEYS_FIXED: &str = "829033be4f407b6c9e472778364fbabb76431c2b";
    const LFS_SHA256_BAD_DIGEST_LEN_FIXED: &str = "60a5483724576389baf6b5cd8300d5ce0c9ae2f3";
    const LFS_SHA256_BAD_DIGEST_CONTENT_FIXED: &str = "ba2c9f71853b64a9460a95a5f6057c4fcabc59a4";
    const LFS_SHA256_EMPTY_DIGEST_FIXED: &str = "b06ca3f0c77173164f9fbec13e1f2a2a6814b69c";
    const LFS_UNKNOWN_ALGO_EMPTY_DIGEST_FIXED: &str = "d7acdabf44ba40589674e59dfce9e5b5515f951c";
    const LFS_MISSING_ALGO_FIXED: &str = "081c396ca642026464c17ad193239316e3100439";
    const LFS_INVALID_KEY_FIXED: &str = "40c8f64bd2f1da90fa7bfbae8aaae9bd654f94ca";
    const LFS_EMPTY_KEY_FIXED: &str = "c4e2d178e6cfd82a82d7b9b68544a7e1ec50d17b";
    const LFS_NONINT_SIZE_FIXED: &str = "442f3131aecf8993912ec0e26e48fa48ad924364";
    const LFS_ZERO_SIZE_FIXED: &str = "e854ba50135b51b59dfd4b59cfcc6a8aae49b01b";

    #[test]
    fn test_lfs_pointer_builder_default() {
        assert!(LfsPointer::builder().build().is_ok());
    }

    #[test]
    fn test_lfs_pointer_name_commit() {
        let check = LfsPointer::default();
        assert_eq!(Check::name(&check), "lfs-pointer");
    }

    #[test]
    fn test_lfs_pointer_name_topic() {
        let check = LfsPointer::default();
        assert_eq!(TopicCheck::name(&check), "lfs-pointer");
    }

    #[test]
    fn test_lfs_invalid_utf8() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_invalid_utf8", LFS_INVALID_UTF8, check);
        test_result_errors(result, &[
            "commit c413d8954d903557de00be9d96b4daa264bd4b22 not allowed; invalid utf-8 sequence \
             in an LFS pointer added in `invalid-utf8.lfs`.",
        ]);
    }

    #[test]
    fn test_lfs_invalid_utf8_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_invalid_utf8_topic", LFS_INVALID_UTF8, check);
        test_result_errors(
            result,
            &["invalid utf-8 sequence in an LFS pointer added in `invalid-utf8.lfs`."],
        );
    }

    #[test]
    fn test_lfs_invalid_utf8_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_invalid_utf8_topic_fixed",
            LFS_INVALID_UTF8_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_invalid_line_format() {
        let check = LfsPointer::default();
        let result = run_check(
            "test_lfs_invalid_line_format",
            LFS_INVALID_LINE_FORMAT,
            check,
        );
        test_result_errors(result, &[
            "commit 709c2cd38863717231453bb4945897a0ebef3f80 not allowed; invalid line in an LFS \
             pointer added in `invalid-line-format.lfs` on line 2.",
            "commit 709c2cd38863717231453bb4945897a0ebef3f80 not allowed; invalid line in an LFS \
             pointer added in `invalid-line-format.lfs` on line 3.",
            "commit 709c2cd38863717231453bb4945897a0ebef3f80 not allowed; invalid line in an LFS \
             pointer added in `invalid-line-format.lfs` on line 4.",
            "commit 709c2cd38863717231453bb4945897a0ebef3f80 not allowed; invalid line in an LFS \
             pointer added in `invalid-line-format.lfs` on line 8.",
        ]);
    }

    #[test]
    fn test_lfs_invalid_line_format_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_invalid_line_format_topic",
            LFS_INVALID_LINE_FORMAT,
            check,
        );
        test_result_errors(
            result,
            &[
                "invalid line in an LFS pointer added in `invalid-line-format.lfs` on line 2.",
                "invalid line in an LFS pointer added in `invalid-line-format.lfs` on line 3.",
                "invalid line in an LFS pointer added in `invalid-line-format.lfs` on line 4.",
                "invalid line in an LFS pointer added in `invalid-line-format.lfs` on line 8.",
            ],
        );
    }

    #[test]
    fn test_lfs_invalid_line_format_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_invalid_line_format_topic_fixed",
            LFS_INVALID_LINE_FORMAT_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_missing_version() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_missing_version", LFS_MISSING_VERSION, check);
        test_result_errors(result, &[
            "commit 0ec2ab0e229fdcbff18a8bfd55774e4f71b04bbb not allowed; the first key in LFS \
             pointer `missing-version.lfs` must be `version`; found `oid`.",
        ]);
    }

    #[test]
    fn test_lfs_missing_version_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_missing_version_topic", LFS_MISSING_VERSION, check);
        test_result_errors(result, &[
            "the first key in LFS pointer `missing-version.lfs` must be `version`; found `oid`.",
        ]);
    }

    #[test]
    fn test_lfs_missing_version_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_missing_version_topic_fixed",
            LFS_MISSING_VERSION_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_misplaced_version() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_misplaced_version", LFS_MISPLACED_VERSION, check);
        test_result_errors(result, &[
            "commit c86f7b78fd6ca471d97f3861addf3a35c25f3504 not allowed; the first key in LFS \
             pointer `misplaced-version.lfs` must be `version`; found `oid`.",
        ]);
    }

    #[test]
    fn test_lfs_misplaced_version_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_misplaced_version_topic",
            LFS_MISPLACED_VERSION,
            check,
        );
        test_result_errors(result, &[
            "the first key in LFS pointer `misplaced-version.lfs` must be `version`; found `oid`.",
        ]);
    }

    #[test]
    fn test_lfs_misplaced_version_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_misplaced_version_topic_fixed",
            LFS_MISPLACED_VERSION_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_invalid_version() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_invalid_version", LFS_INVALID_VERSION, check);
        test_result_warnings(
            result,
            &[
                "commit f799c407f7678ba38f87720fb4217ece6c50f7e5 contains an unexpected git-lfs \
                 version string in `invalid-version.lfs`: `https://hawser.github.com/spec/v1`.",
            ],
        );
    }

    #[test]
    fn test_lfs_invalid_version_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_invalid_version_topic", LFS_INVALID_VERSION, check);
        test_result_warnings(
            result,
            &[
                "unexpected git-lfs version string in `invalid-version.lfs`: \
                 `https://hawser.github.com/spec/v1`.",
            ],
        );
    }

    #[test]
    fn test_lfs_invalid_version_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_invalid_version_topic_fixed",
            LFS_INVALID_VERSION_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_duplicate_key() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_duplicate_key", LFS_DUPLICATE_KEY, check);
        test_result_errors(
            result,
            &[
                "commit 4d0ead1cac518874d1f7cd005569238b7b9bc7c3 not allowed; duplicate key \
                 `duplicate` in an LFS pointer added in `duplicate-key.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_duplicate_key_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_duplicate_key_topic", LFS_DUPLICATE_KEY, check);
        test_result_errors(
            result,
            &["duplicate key `duplicate` in an LFS pointer added in `duplicate-key.lfs`."],
        );
    }

    #[test]
    fn test_lfs_duplicate_key_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_duplicate_key_topic_fixed",
            LFS_DUPLICATE_KEY_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_missing_size() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_missing_size", LFS_MISSING_SIZE, check);
        test_result_errors(
            result,
            &[
                "commit e6f342338093141e365b1bb1596290c155198c60 not allowed; an LFS pointer is \
                 missing the `size` key in `missing-size.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_missing_size_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_missing_size_topic", LFS_MISSING_SIZE, check);
        test_result_errors(
            result,
            &["an LFS pointer is missing the `size` key in `missing-size.lfs`."],
        );
    }

    #[test]
    fn test_lfs_missing_size_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_missing_size_topic_fixed",
            LFS_MISSING_SIZE_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_missing_oid() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_missing_oid", LFS_MISSING_OID, check);
        test_result_errors(
            result,
            &[
                "commit ceeabe41a44a4469b6644017af725b0127d92302 not allowed; an LFS pointer is \
                 missing the `oid` key in `missing-oid.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_missing_oid_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_missing_oid_topic", LFS_MISSING_OID, check);
        test_result_errors(
            result,
            &["an LFS pointer is missing the `oid` key in `missing-oid.lfs`."],
        );
    }

    #[test]
    fn test_lfs_missing_oid_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_missing_oid_topic_fixed",
            LFS_MISSING_OID_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_unrecognized_hash() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_unrecognized_hash", LFS_UNRECOGNIZED_HASH, check);
        test_result_warnings(
            result,
            &[
                "commit 1aa2873adebe911caf594c376efc5c522f8d4c3a contains an unrecognized hash \
                 algorithm `md256` in LFS pointer `unrecognized-hash.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_unrecognized_hash_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_unrecognized_hash_topic",
            LFS_UNRECOGNIZED_HASH,
            check,
        );
        test_result_warnings(
            result,
            &["unrecognized hash algorithm `md256` in LFS pointer `unrecognized-hash.lfs`."],
        );
    }

    #[test]
    fn test_lfs_unrecognized_hash_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_unrecognized_hash_topic_fixed",
            LFS_UNRECOGNIZED_HASH_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_missing_hash() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_missing_hash", LFS_MISSING_HASH, check);
        test_result_errors(
            result,
            &[
                "commit 109806f98fd036cba88d07f94df162aad5086d0b not allowed; missing digest for \
                 cdbd0acd629e920c87bb4d4ff1f5e85ca9393b61629ef7b2bd64b1a7e1fe4b96 algorithm in \
                 LFS pointer `missing-hash.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_missing_hash_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_missing_hash_topic", LFS_MISSING_HASH, check);
        test_result_errors(result, &[
            "missing digest for cdbd0acd629e920c87bb4d4ff1f5e85ca9393b61629ef7b2bd64b1a7e1fe4b96 \
             algorithm in LFS pointer `missing-hash.lfs`.",
        ]);
    }

    #[test]
    fn test_lfs_missing_hash_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_missing_hash_topic_fixed",
            LFS_MISSING_HASH_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_unsorted_keys() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_unsorted_keys", LFS_UNSORTED_KEYS, check);
        test_result_errors(
            result,
            &[
                "commit aaeedde2f188005c24e090bc44378b17770e3b94 not allowed; unsorted key `oid` \
                 found in LFS pointer `unsorted-keys.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_unsorted_keys_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_unsorted_keys_topic", LFS_UNSORTED_KEYS, check);
        test_result_errors(
            result,
            &["unsorted key `oid` found in LFS pointer `unsorted-keys.lfs`."],
        );
    }

    #[test]
    fn test_lfs_unsorted_keys_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_unsorted_keys_topic_fixed",
            LFS_UNSORTED_KEYS_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_empty_pointer() {
        let check = LfsPointer::default();
        run_check_ok("test_lfs_empty_pointer", LFS_EMPTY_POINTER, check);
    }

    #[test]
    fn test_lfs_empty_pointer_topic() {
        let check = LfsPointer::default();
        run_topic_check_ok("test_lfs_empty_pointer_topic", LFS_EMPTY_POINTER, check)
    }

    #[test]
    fn test_lfs_valid_pointer() {
        let check = LfsPointer::default();
        run_check_ok("test_lfs_valid_pointer", LFS_VALID_POINTER, check);
    }

    #[test]
    fn test_lfs_valid_pointer_topic() {
        let check = LfsPointer::default();
        run_topic_check_ok("test_lfs_valid_pointer_topic", LFS_VALID_POINTER, check)
    }

    #[test]
    fn test_lfs_sha256_bad_digest_len() {
        let check = LfsPointer::default();
        let result = run_check(
            "test_lfs_sha256_bad_digest_len",
            LFS_SHA256_BAD_DIGEST_LEN,
            check,
        );
        test_result_errors(
            result,
            &[
                "commit b00442c09d00aeb1ffabffea821e8b286da6b0df not allowed; invalid digest \
                 length in `deadbeefdeadbeefdeadbeefdeadbeef` for sha256 algorithm in LFS pointer \
                 `sha256-digest-len-mismatch.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_sha256_bad_digest_len_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_sha256_bad_digest_len_topic",
            LFS_SHA256_BAD_DIGEST_LEN,
            check,
        );
        test_result_errors(
            result,
            &[
                "invalid digest length in `deadbeefdeadbeefdeadbeefdeadbeef` for sha256 algorithm \
                 in LFS pointer `sha256-digest-len-mismatch.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_sha256_bad_digest_len_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_sha256_bad_digest_len_topic_fixed",
            LFS_SHA256_BAD_DIGEST_LEN_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_sha256_bad_digest_content() {
        let check = LfsPointer::default();
        let result = run_check(
            "test_lfs_sha256_bad_digest_content",
            LFS_SHA256_BAD_DIGEST_CONTENT,
            check,
        );
        test_result_errors(
            result,
            &[
                "commit 1cd551597d65f74a57928c024ff58edb532767e0 not allowed; invalid digest \
                 content `deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefasciihex` for \
                 sha256 algorithm in LFS pointer `sha256-digest-content.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_sha256_bad_digest_content_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_sha256_bad_digest_content_topic",
            LFS_SHA256_BAD_DIGEST_CONTENT,
            check,
        );
        test_result_errors(
            result,
            &["invalid digest content \
               `deadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefdeadbeefasciihex` for \
               sha256 algorithm in LFS pointer `sha256-digest-content.lfs`."],
        );
    }

    #[test]
    fn test_lfs_sha256_bad_digest_content_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_sha256_bad_digest_content_topic_fixed",
            LFS_SHA256_BAD_DIGEST_CONTENT_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_sha256_empty_digest() {
        let check = LfsPointer::default();
        let result = run_check(
            "test_lfs_sha256_empty_digest",
            LFS_SHA256_EMPTY_DIGEST,
            check,
        );
        test_result_errors(
            result,
            &[
                "commit 6d377c8197b372df64689cb90db70904a2667dc9 not allowed; empty digest for \
                 sha256 algorithm in LFS pointer `sha256-digest-empty.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_sha256_empty_digest_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_sha256_empty_digest_topic",
            LFS_SHA256_EMPTY_DIGEST,
            check,
        );
        test_result_errors(
            result,
            &["empty digest for sha256 algorithm in LFS pointer `sha256-digest-empty.lfs`."],
        );
    }

    #[test]
    fn test_lfs_sha256_empty_digest_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_sha256_empty_digest_topic_fixed",
            LFS_SHA256_EMPTY_DIGEST_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_unknown_algo_empty_digest() {
        let check = LfsPointer::default();
        let result = run_check(
            "test_lfs_unknown_algo_empty_digest",
            LFS_UNKNOWN_ALGO_EMPTY_DIGEST,
            check,
        );

        assert_eq!(result.warnings().len(), 1);
        assert_eq!(
            result.warnings()[0],
            "commit 96c193edaf748309b7bb5c2e816abda4a06c80c0 contains an unrecognized hash \
             algorithm `notanalgo` in LFS pointer `unknown-digest-empty.lfs`.",
        );
        assert_eq!(result.alerts().len(), 0);
        assert_eq!(result.errors().len(), 1);
        assert_eq!(
            result.errors()[0],
            "commit 96c193edaf748309b7bb5c2e816abda4a06c80c0 not allowed; empty digest for \
             notanalgo algorithm in LFS pointer `unknown-digest-empty.lfs`.",
        );
        assert!(!result.temporary());
        assert!(!result.allowed());
        assert!(!result.pass());
    }

    #[test]
    fn test_lfs_unknown_algo_empty_digest_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_unknown_algo_empty_digest_topic",
            LFS_UNKNOWN_ALGO_EMPTY_DIGEST,
            check,
        );

        assert_eq!(result.warnings().len(), 1);
        assert_eq!(
            result.warnings()[0],
            "unrecognized hash algorithm `notanalgo` in LFS pointer `unknown-digest-empty.lfs`.",
        );
        assert_eq!(result.alerts().len(), 0);
        assert_eq!(result.errors().len(), 1);
        assert_eq!(
            result.errors()[0],
            "empty digest for notanalgo algorithm in LFS pointer `unknown-digest-empty.lfs`.",
        );
        assert!(!result.temporary());
        assert!(!result.allowed());
        assert!(!result.pass());
    }

    #[test]
    fn test_lfs_unknown_algo_empty_digest_topic_fixed() {
        let check = LfsPointer::default();
        let result = run_topic_check(
            "test_lfs_unknown_algo_empty_digest_topic_fixed",
            LFS_UNKNOWN_ALGO_EMPTY_DIGEST_FIXED,
            check,
        );

        test_result_warnings(
            result,
            &["unrecognized hash algorithm `notanalgo` in LFS pointer \
               `unknown-digest-empty.lfs`."],
        );
    }

    #[test]
    fn test_lfs_missing_algo() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_missing_algo", LFS_MISSING_ALGO, check);
        test_result_errors(
            result,
            &[
                "commit 6b4125e802283d716ceef0f4c7630a94810654fd not allowed; missing hash \
                 algorithm in LFS pointer `missing-oid-algo.lfs`.",
            ],
        );
    }

    #[test]
    fn test_lfs_missing_algo_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_missing_algo_topic", LFS_MISSING_ALGO, check);
        test_result_errors(
            result,
            &["missing hash algorithm in LFS pointer `missing-oid-algo.lfs`."],
        );
    }

    #[test]
    fn test_lfs_missing_algo_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_missing_algo_topic_fixed",
            LFS_MISSING_ALGO_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_invalid_key() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_invalid_key", LFS_INVALID_KEY, check);
        test_result_errors(
            result,
            &[
                "commit 335b860660b133c59c651b3cb22ac883fdb8d004 not allowed; invalid line in an \
                 LFS pointer added in `invalid-key.lfs` on line 2.",
            ],
        );
    }

    #[test]
    fn test_lfs_invalid_key_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_invalid_key_topic", LFS_INVALID_KEY, check);
        test_result_errors(
            result,
            &["invalid line in an LFS pointer added in `invalid-key.lfs` on line 2."],
        );
    }

    #[test]
    fn test_lfs_invalid_key_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_invalid_key_topic_fixed",
            LFS_INVALID_KEY_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_empty_key() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_empty_key", LFS_EMPTY_KEY, check);
        test_result_ok(result);
    }

    #[test]
    fn test_lfs_empty_key_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_empty_key_topic", LFS_EMPTY_KEY, check);
        test_result_ok(result);
    }

    #[test]
    fn test_lfs_empty_key_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok("test_lfs_empty_key_topic_fixed", LFS_EMPTY_KEY_FIXED, check)
    }

    #[test]
    fn test_lfs_delete_file() {
        let check = LfsPointer::default();
        let conf = make_check_conf(&check);

        let result = test_check_base(
            "test_lfs_delete_file",
            LFS_DELETED_BAD_FILE,
            LFS_INVALID_UTF8,
            &conf,
        );
        test_result_ok(result);
    }

    #[test]
    fn test_lfs_delete_file_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_delete_file_topic", LFS_DELETED_BAD_FILE, check);
        test_result_ok(result);
    }

    #[test]
    fn test_lfs_invalid_size() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_invalid_size", LFS_NONINT_SIZE, check);
        test_result_errors(result, &[
            "commit 3ed812f83b9bd5d8ed5538933254d07a42928e47 not allowed; the `size` key value in \
             LFS pointer `invalid-size.lfs` must be an unsigned integer; found `notanint`.",
        ]);
    }

    #[test]
    fn test_lfs_invalid_size_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_invalid_size_topic", LFS_NONINT_SIZE, check);
        test_result_errors(result, &[
            "the `size` key value in LFS pointer `invalid-size.lfs` must be an unsigned integer; \
             found `notanint`.",
        ]);
    }

    #[test]
    fn test_lfs_invalid_size_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok(
            "test_lfs_invalid_size_topic_fixed",
            LFS_NONINT_SIZE_FIXED,
            check,
        )
    }

    #[test]
    fn test_lfs_zero_size() {
        let check = LfsPointer::default();
        let result = run_check("test_lfs_zero_size", LFS_ZERO_SIZE, check);
        test_result_errors(
            result,
            &[
                "commit 5523b49074a66611aaafcac32cf7d05a654f552c not allowed; the `size` value in \
                 LFS pointer `zero-size.lfs` must be greater than 0; found `0`.",
            ],
        );
    }

    #[test]
    fn test_lfs_zero_size_topic() {
        let check = LfsPointer::default();
        let result = run_topic_check("test_lfs_zero_size_topic", LFS_ZERO_SIZE, check);
        test_result_errors(result, &[
            "the `size` value in LFS pointer `zero-size.lfs` must be greater than 0; found `0`.",
        ]);
    }

    #[test]
    fn test_lfs_zero_size_topic_fixed() {
        let check = LfsPointer::default();
        run_topic_check_ok("test_lfs_zero_size_topic_fixed", LFS_ZERO_SIZE_FIXED, check)
    }

    #[test]
    fn test_lfs_not_lfs_filter() {
        let check = LfsPointer::default();
        run_check_ok("test_lfs_not_lfs_filter", LFS_NOT_LFS_FILTER, check);
    }

    #[test]
    fn test_lfs_not_lfs_filter_topic() {
        let check = LfsPointer::default();
        run_topic_check_ok("test_lfs_not_lfs_filter_topic", LFS_NOT_LFS_FILTER, check);
    }
}
