// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]
// XXX(rust-1.66)
#![allow(clippy::uninlined_format_args)]

//! Basic Checks
//!
//! Simple checks useful in a wide variety of projects.

mod binary_format;

mod allow_robot;
pub use self::allow_robot::AllowRobot;

mod bad_commit;
pub use self::bad_commit::BadCommit;

mod bad_commits;
pub use self::bad_commits::BadCommits;

mod changelog;
pub use self::changelog::Changelog;
pub use self::changelog::ChangelogStyle;

mod check_end_of_line;
pub use self::check_end_of_line::CheckEndOfLine;

mod check_executable_permissions;
pub use self::check_executable_permissions::CheckExecutablePermissions;

mod check_size;
pub use self::check_size::CheckSize;

mod check_whitespace;
pub use self::check_whitespace::CheckWhitespace;

mod commit_subject;
pub use self::commit_subject::CommitSubject;

mod fast_forward;
pub use self::fast_forward::FastForward;

mod formatting;
pub use self::formatting::Formatting;

mod invalid_paths;
pub use self::invalid_paths::InvalidPaths;

mod invalid_utf8;
pub use self::invalid_utf8::InvalidUtf8;

mod lfs_pointer;
pub use self::lfs_pointer::LfsPointer;

mod reject_bidi;
pub use self::reject_bidi::RejectBiDi;

mod reject_binaries;
pub use self::reject_binaries::RejectBinaries;

mod reject_conflict_paths;
pub use self::reject_conflict_paths::RejectConflictPaths;

mod reject_merges;
pub use self::reject_merges::RejectMerges;

mod reject_separate_root;
pub use self::reject_separate_root::RejectSeparateRoot;

mod reject_symlinks;
pub use self::reject_symlinks::RejectSymlinks;

mod release_branch;
pub use self::release_branch::ReleaseBranch;

mod restricted_path;
pub use self::restricted_path::RestrictedPath;

mod submodule_available;
pub use self::submodule_available::SubmoduleAvailable;

mod submodule_rewind;
pub use self::submodule_rewind::SubmoduleRewind;

mod submodule_watch;
pub use self::submodule_watch::SubmoduleWatch;

mod third_party;
pub use self::third_party::ThirdParty;

mod valid_name;
pub use self::valid_name::ValidName;
pub use self::valid_name::ValidNameFullNamePolicy;

/// Module for all builders for checks.
///
/// This module is here for documentation purposes. Each check's `builder()` method should be
/// used instead.
pub mod builders {
    pub use crate::allow_robot::AllowRobotBuilder;
    pub use crate::bad_commit::BadCommitBuilder;
    pub use crate::bad_commits::BadCommitsBuilder;
    pub use crate::changelog::ChangelogBuilder;
    pub use crate::check_end_of_line::CheckEndOfLineBuilder;
    pub use crate::check_executable_permissions::CheckExecutablePermissionsBuilder;
    pub use crate::check_size::CheckSizeBuilder;
    pub use crate::check_whitespace::CheckWhitespaceBuilder;
    pub use crate::commit_subject::CommitSubjectBuilder;
    pub use crate::fast_forward::FastForwardBuilder;
    pub use crate::formatting::FormattingBuilder;
    pub use crate::invalid_paths::InvalidPathsBuilder;
    pub use crate::invalid_utf8::InvalidUtf8Builder;
    pub use crate::lfs_pointer::LfsPointerBuilder;
    pub use crate::reject_bidi::RejectBiDiBuilder;
    pub use crate::reject_binaries::RejectBinariesBuilder;
    pub use crate::reject_conflict_paths::RejectConflictPathsBuilder;
    pub use crate::reject_merges::RejectMergesBuilder;
    pub use crate::reject_separate_root::RejectSeparateRootBuilder;
    pub use crate::reject_symlinks::RejectSymlinksBuilder;
    pub use crate::release_branch::ReleaseBranchBuilder;
    pub use crate::restricted_path::RestrictedPathBuilder;
    pub use crate::submodule_available::SubmoduleAvailableBuilder;
    pub use crate::submodule_rewind::SubmoduleRewindBuilder;
    pub use crate::submodule_watch::SubmoduleWatchBuilder;
    pub use crate::third_party::ThirdPartyBuilder;
    pub use crate::valid_name::ValidNameBuilder;
}

/// Configuration structures for checks.
///
/// These structures are registered using `inventory` using the `git-checks-config` crate. They
/// are offered here for documentation purposes mainly, but also in case their specific
/// implementations are useful.
#[cfg(feature = "config")]
pub mod config {
    pub use crate::allow_robot::config::AllowRobotConfig;
    pub use crate::bad_commit::config::BadCommitConfig;
    pub use crate::bad_commits::config::BadCommitsConfig;
    pub use crate::changelog::config::ChangelogConfig;
    pub use crate::check_end_of_line::config::CheckEndOfLineConfig;
    pub use crate::check_executable_permissions::config::CheckExecutablePermissionsConfig;
    pub use crate::check_size::config::CheckSizeConfig;
    pub use crate::check_whitespace::config::CheckWhitespaceConfig;
    pub use crate::commit_subject::config::CommitSubjectConfig;
    pub use crate::formatting::config::FormattingConfig;
    pub use crate::invalid_paths::config::InvalidPathsConfig;
    pub use crate::invalid_utf8::config::InvalidUtf8Config;
    pub use crate::lfs_pointer::config::LfsPointerConfig;
    pub use crate::reject_bidi::config::RejectBiDiConfig;
    pub use crate::reject_binaries::config::RejectBinariesConfig;
    pub use crate::reject_conflict_paths::config::RejectConflictPathsConfig;
    pub use crate::reject_merges::config::RejectMergesConfig;
    pub use crate::reject_separate_root::config::RejectSeparateRootConfig;
    pub use crate::reject_symlinks::config::RejectSymlinksConfig;
    pub use crate::release_branch::config::ReleaseBranchConfig;
    pub use crate::restricted_path::config::RestrictedPathConfig;
    pub use crate::submodule_available::config::SubmoduleAvailableConfig;
    pub use crate::submodule_rewind::config::SubmoduleRewindConfig;
    pub use crate::submodule_watch::config::SubmoduleWatchConfig;
    pub use crate::third_party::config::ThirdPartyConfig;
    pub use crate::valid_name::config::ValidNameConfig;
}

#[cfg(test)]
pub mod test;
