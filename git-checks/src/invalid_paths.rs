// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;
use git_checks_core::impl_prelude::*;

/// A check which denies commits which adds files containing special characters in their paths.
#[derive(Builder, Debug, Default, Clone)]
#[builder(field(private))]
pub struct InvalidPaths {
    /// Characters not allowed within a filename.
    ///
    /// In addition to whitespace, control, and non-ASCII characters, which are always disallowed.
    ///
    /// Configuration: Optional
    /// Default: `String::new()
    #[builder(setter(into))]
    #[builder(default)]
    invalid_characters: String,
    /// Allow the space character (ASCII x20)
    ///
    /// This allows for exempting ` ` in paths.
    ///
    /// Configuration: Optional
    /// Default: `false`
    #[builder(default = "false")]
    allow_space: bool,
    /// Enforce Windows-specific path rules.
    ///
    /// This includes a list of reserved names as well as rejecting path components which end in
    /// `.` or ` ` (space).
    ///
    /// Configuration: Optional
    /// Default: `false`
    #[builder(default = "false")]
    enforce_windows_rules: bool,
}

const WINDOWS_FORBIDDEN_ASCII: &[u8] = br#"<>:"\|?*"#;
const WINDOWS_FORBIDDEN_NAMES: &[&[u8]] = &[
    b"CON", b"PRN", b"AUX", b"NUL", b"COM1", b"COM2", b"COM3", b"COM4", b"COM5", b"COM6", b"COM7",
    b"COM8", b"COM9", b"LPT1", b"LPT2", b"LPT3", b"LPT4", b"LPT5", b"LPT6", b"LPT7", b"LPT8",
    b"LPT9",
];

impl InvalidPaths {
    /// Create a new builder.
    pub fn builder() -> InvalidPathsBuilder {
        InvalidPathsBuilder::default()
    }

    fn disallowed_whitespace(&self, c: char) -> bool {
        c.is_whitespace() && (!self.allow_space || c != ' ')
    }

    /// Whether a string has any invalid characters.
    fn has_invalid_chars(&self, chars: &str) -> bool {
        chars.chars().any(|ref c| {
            !c.is_ascii()
                || self.disallowed_whitespace(*c)
                || c.is_control()
                || self.invalid_characters.contains(*c)
        })
    }

    /// Whether the string has any invalid bytes.
    fn has_invalid_bytes(&self, bytes: &[u8]) -> bool {
        bytes.iter().any(|&c| !c.is_ascii() || c < 32)
    }

    /// Whether the string has any invalid bytes.
    ///
    /// See https://docs.microsoft.com/en-us/windows/desktop/FileIO/naming-a-file for rules.
    fn is_valid_for_windows(chars: &[u8]) -> bool {
        // Check for invalid characters.
        !chars.iter().any(|c| WINDOWS_FORBIDDEN_ASCII.contains(c)) &&
            // Check component names.
            !chars.split(|&b| b == b'/')
                .any(|name| {
                    name.ends_with(b" ") ||
                        name.ends_with(b".") ||
                        WINDOWS_FORBIDDEN_NAMES.contains(&name)
                })
    }
}

impl ContentCheck for InvalidPaths {
    fn name(&self) -> &str {
        "invalid-paths"
    }

    fn check(
        &self,
        _: &CheckGitContext,
        content: &dyn Content,
    ) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        for diff in content.diffs() {
            if let StatusChange::Added = diff.status {
                let have_bad_char = self.has_invalid_chars(diff.name.as_str());
                let have_bad_bytes = self.has_invalid_bytes(diff.name.as_bytes());

                if have_bad_char || have_bad_bytes {
                    result.add_error(format!(
                        "{}adds the `{}` path which contains at least one forbidden character: \
                         `<non-ASCII><whitespace><control>{}`.",
                        commit_prefix(content),
                        diff.name,
                        self.invalid_characters,
                    ));
                }

                if self.enforce_windows_rules && !Self::is_valid_for_windows(diff.name.as_bytes()) {
                    result.add_error(format!(
                        "{}adds the `{}` path which is invalid on Windows.",
                        commit_prefix(content),
                        diff.name,
                    ));
                }
            }
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use git_checks_config::{register_checks, CommitCheckConfig, IntoCheck, TopicCheckConfig};
    use serde::Deserialize;
    #[cfg(test)]
    use serde_json::json;

    use crate::InvalidPaths;

    /// Configuration for the `InvalidPaths` check.
    ///
    /// The `invalid_characters` key is a string containing characters which are not allowed to
    /// appear in paths. By default, its value is `"<>:\"|?*"` which excludes characters not
    /// allowed in paths on Windows. The `allow_space` key is a boolean defaulting to false which
    /// specifies whether ASCII space is allowed (whitespace is otherwise disallowed).
    ///
    /// This check is registered as a commit check with the name `"invalid_paths"` and a topic
    /// check with the name `"invalid_paths/topic"`.
    ///
    /// # Example
    ///
    /// ```json
    /// {
    ///     "invalid_characters": "<>:\"|?*",
    ///     "allow_space": false
    /// }
    /// ```
    #[derive(Deserialize, Debug)]
    pub struct InvalidPathsConfig {
        #[serde(default)]
        invalid_characters: Option<String>,
        #[serde(default)]
        allow_space: Option<bool>,
        #[serde(default)]
        enforce_windows_rules: Option<bool>,
    }

    impl IntoCheck for InvalidPathsConfig {
        type Check = InvalidPaths;

        fn into_check(self) -> Self::Check {
            let mut builder = InvalidPaths::builder();

            if let Some(invalid_characters) = self.invalid_characters {
                builder.invalid_characters(invalid_characters);
            }

            if let Some(allow_space) = self.allow_space {
                builder.allow_space(allow_space);
            }

            if let Some(enforce_windows_rules) = self.enforce_windows_rules {
                builder.enforce_windows_rules(enforce_windows_rules);
            }

            builder
                .build()
                .expect("configuration mismatch for `InvalidPaths`")
        }
    }

    register_checks! {
        InvalidPathsConfig {
            "invalid_paths" => CommitCheckConfig,
            "invalid_paths/topic" => TopicCheckConfig,
        },
    }

    #[test]
    fn test_invalid_paths_config_empty() {
        let json = json!({});
        let check: InvalidPathsConfig = serde_json::from_value(json).unwrap();

        assert_eq!(check.invalid_characters, None);
        assert_eq!(check.allow_space, None);
        assert_eq!(check.enforce_windows_rules, None);

        let check = check.into_check();

        assert_eq!(check.invalid_characters, "");
        assert!(!check.allow_space);
        assert!(!check.enforce_windows_rules);
    }

    #[test]
    fn test_invalid_paths_config_all_fields() {
        let json = json!({
            "invalid_characters": "abc",
            "allow_space": true,
            "enforce_windows_rules": true,
        });
        let check: InvalidPathsConfig = serde_json::from_value(json).unwrap();

        assert_eq!(check.invalid_characters, Some("abc".into()));
        assert_eq!(check.allow_space, Some(true));
        assert_eq!(check.enforce_windows_rules, Some(true));

        let check = check.into_check();

        assert_eq!(check.invalid_characters, "abc");
        assert!(check.allow_space);
        assert!(check.enforce_windows_rules);
    }
}

#[cfg(test)]
mod tests {
    use git_checks_core::{Check, TopicCheck};

    use crate::test::*;
    use crate::InvalidPaths;

    const BAD_TOPIC: &str = "f536f44cf96b82e479d4973d5ea1cf78058bd1fb";
    const FIX_TOPIC: &str = "8ff69e1834ef2e82c0ed5cfb4ba56f1e4de85d03";
    const BAD_WINDOWS_TOPIC: &str = "6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5";
    const FIX_WINDOWS_TOPIC: &str = "e2130533af8b9c274c82ad8cdede5fdbb8053c7b";

    #[test]
    fn test_allow_space() {
        let mut builder = InvalidPaths::builder();
        let check = builder.build().unwrap();
        assert!(!check.disallowed_whitespace('a'));
        assert!(check.disallowed_whitespace(' '));
        assert!(check.disallowed_whitespace('\n'));
        builder.allow_space(true);
        let check = builder.build().unwrap();
        assert!(!check.disallowed_whitespace('a'));
        assert!(!check.disallowed_whitespace(' '));
        assert!(check.disallowed_whitespace('\n'));
    }

    #[test]
    fn test_valid_windows_characters() {
        assert!(!InvalidPaths::is_valid_for_windows(b"with<langle"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with>rangle"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with>rangle"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with:colon"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with\"dquote"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with\\slash"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with|vbar"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with?question"));
        assert!(!InvalidPaths::is_valid_for_windows(b"with*asterisk"));
    }

    #[test]
    fn test_valid_windows_suffixes() {
        assert!(!InvalidPaths::is_valid_for_windows(b"ends.with."));
        assert!(!InvalidPaths::is_valid_for_windows(b"ends.with "));
        assert!(!InvalidPaths::is_valid_for_windows(b"dir.ends.with./file"));
        assert!(!InvalidPaths::is_valid_for_windows(b"dir.ends.with /file"));
    }

    #[test]
    fn test_valid_windows_reserved_names_as_files() {
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/CON"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/PRN"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/AUX"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/NUL"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM1"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM2"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM3"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM4"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM5"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM6"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM7"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM8"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/COM9"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT1"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT2"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT3"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT4"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT5"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT6"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT7"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT8"));
        assert!(!InvalidPaths::is_valid_for_windows(b"forbidden/name/LPT9"));
    }

    #[test]
    fn test_valid_windows_reserved_names_as_dirs() {
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/CON/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/PRN/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/AUX/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/NUL/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM1/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM2/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM3/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM4/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM5/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM6/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM7/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM8/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/COM9/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT1/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT2/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT3/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT4/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT5/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT6/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT7/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT8/as/dir",
        ));
        assert!(!InvalidPaths::is_valid_for_windows(
            b"forbidden/name/LPT9/as/dir",
        ));
    }

    #[test]
    fn test_invalid_paths_builder_default() {
        assert!(InvalidPaths::builder().build().is_ok());
    }

    #[test]
    fn test_invalid_paths_name_commit() {
        let check = InvalidPaths::default();
        assert_eq!(Check::name(&check), "invalid-paths");
    }

    #[test]
    fn test_invalid_paths_name_topic() {
        let check = InvalidPaths::default();
        assert_eq!(TopicCheck::name(&check), "invalid-paths");
    }

    #[test]
    fn test_invalid_paths() {
        let check = InvalidPaths::builder()
            .invalid_characters("$")
            .build()
            .unwrap();
        let result = run_check("test_invalid_paths", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"control-character-\\003\"` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"invalid-utf8-\\200\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"non-ascii-\\303\\251\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `with whitespace` \
             path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `with-dollar-$` path \
             which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_allow_space() {
        let check = InvalidPaths::builder()
            .invalid_characters("$")
            .allow_space(true)
            .build()
            .unwrap();
        let result = run_check("test_invalid_paths_allow_space", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"control-character-\\003\"` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"invalid-utf8-\\200\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"non-ascii-\\303\\251\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `with-dollar-$` path \
             which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_topic() {
        let check = InvalidPaths::builder()
            .invalid_characters("$")
            .build()
            .unwrap();
        let result = run_topic_check("test_invalid_paths_topic", BAD_TOPIC, check);
        test_result_errors(result, &[
            "adds the `\"control-character-\\003\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `\"invalid-utf8-\\200\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `\"non-ascii-\\303\\251\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `with whitespace` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
            "adds the `with-dollar-$` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_topic_allow_space() {
        let check = InvalidPaths::builder()
            .invalid_characters("$")
            .allow_space(true)
            .build()
            .unwrap();
        let result = run_topic_check("test_invalid_paths_topic_allow_space", BAD_TOPIC, check);
        test_result_errors(result, &[
            "adds the `\"control-character-\\003\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `\"invalid-utf8-\\200\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `\"non-ascii-\\303\\251\"` path which contains at least one forbidden \
             character: `<non-ASCII><whitespace><control>$`.",
            "adds the `with-dollar-$` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>$`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_default() {
        let check = InvalidPaths::default();
        let result = run_check("test_invalid_paths_default", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"control-character-\\003\"` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `\"invalid-utf8-\\200\"` \
             path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `\"non-ascii-\\303\\251\"` \
             path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `with whitespace` path \
             which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_default_allow_space() {
        let check = InvalidPaths::builder().allow_space(true).build().unwrap();
        let result = run_check("test_invalid_paths_default_allow_space", BAD_TOPIC, check);
        test_result_errors(result, &[
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the \
             `\"control-character-\\003\"` path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `\"invalid-utf8-\\200\"` \
             path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
            "commit f536f44cf96b82e479d4973d5ea1cf78058bd1fb adds the `\"non-ascii-\\303\\251\"` \
             path which contains at least one forbidden character: \
             `<non-ASCII><whitespace><control>`.",
        ]);
    }

    #[test]
    fn test_invalid_paths_topic_fixed() {
        let check = InvalidPaths::default();
        run_topic_check_ok("test_invalid_paths_topic_fixed", FIX_TOPIC, check);
    }

    #[test]
    fn test_invalid_paths_topic_windows_ignore() {
        let check = InvalidPaths::builder()
            // Ignore the space in the path name testing path suffixes.
            .allow_space(true)
            .build()
            .unwrap();
        run_topic_check_ok(
            "test_invalid_paths_topic_windows_ignore",
            BAD_WINDOWS_TOPIC,
            check,
        );
    }

    #[test]
    fn test_invalid_paths_windows() {
        let check = InvalidPaths::builder()
            .allow_space(true)
            .enforce_windows_rules(true)
            .build()
            .unwrap();
        let result = run_check("test_invalid_paths_windows", BAD_WINDOWS_TOPIC, check);
        test_result_errors(
            result,
            &[
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `"invalid-char-\"-dir/subdir"` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `"invalid-char-\"-file"` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-*-dir/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-*-file` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-:-dir/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-:-file` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-<-dir/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-<-file` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char->-dir/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char->-file` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-?-dir/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-char-?-file` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `"invalid-char-\\-dir/subdir"` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `"invalid-char-\\-file"` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/AUX/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM1/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM2/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM3/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM4/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM5/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM6/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM7/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM8/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/COM9/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/CON/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT1/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT2/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT3/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT4/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT5/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT6/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT7/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT8/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/LPT9/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/NUL/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/dirs/PRN/subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/AUX` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM1` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM2` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM3` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM4` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM5` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM6` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM7` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM8` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/COM9` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/CON` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT1` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT2` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT3` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT4` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT5` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT6` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT7` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT8` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/LPT9` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/NUL` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-names/files/PRN` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-suffix-dir- /subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-suffix-dir-./subdir` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-suffix-file- ` path which is invalid on Windows."#,
                r#"commit 6354b8d1b1c247d0e35faf2de3ab1be0b7e582a5 adds the `invalid-suffix-file-.` path which is invalid on Windows."#,
            ],
        );
    }

    #[test]
    fn test_invalid_paths_windows_topic() {
        let check = InvalidPaths::builder()
            .allow_space(true)
            .enforce_windows_rules(true)
            .build()
            .unwrap();
        let result = run_topic_check("test_invalid_paths_windows_topic", BAD_WINDOWS_TOPIC, check);
        test_result_errors(
            result,
            &[
                r#"adds the `"invalid-char-\"-dir/subdir"` path which is invalid on Windows."#,
                r#"adds the `"invalid-char-\"-file"` path which is invalid on Windows."#,
                r#"adds the `invalid-char-*-dir/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-char-*-file` path which is invalid on Windows."#,
                r#"adds the `invalid-char-:-dir/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-char-:-file` path which is invalid on Windows."#,
                r#"adds the `invalid-char-<-dir/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-char-<-file` path which is invalid on Windows."#,
                r#"adds the `invalid-char->-dir/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-char->-file` path which is invalid on Windows."#,
                r#"adds the `invalid-char-?-dir/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-char-?-file` path which is invalid on Windows."#,
                r#"adds the `"invalid-char-\\-dir/subdir"` path which is invalid on Windows."#,
                r#"adds the `"invalid-char-\\-file"` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/AUX/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM1/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM2/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM3/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM4/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM5/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM6/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM7/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM8/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/COM9/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/CON/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT1/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT2/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT3/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT4/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT5/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT6/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT7/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT8/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/LPT9/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/NUL/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/dirs/PRN/subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/AUX` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM1` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM2` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM3` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM4` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM5` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM6` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM7` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM8` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/COM9` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/CON` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT1` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT2` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT3` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT4` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT5` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT6` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT7` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT8` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/LPT9` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/NUL` path which is invalid on Windows."#,
                r#"adds the `invalid-names/files/PRN` path which is invalid on Windows."#,
                r#"adds the `invalid-suffix-dir- /subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-suffix-dir-./subdir` path which is invalid on Windows."#,
                r#"adds the `invalid-suffix-file- ` path which is invalid on Windows."#,
                r#"adds the `invalid-suffix-file-.` path which is invalid on Windows."#,
            ],
        );
    }

    #[test]
    fn test_invalid_paths_windows_topic_fixed() {
        let check = InvalidPaths::builder()
            .enforce_windows_rules(true)
            .build()
            .unwrap();
        run_topic_check_ok(
            "test_invalid_paths_windows_topic_fixed",
            FIX_WINDOWS_TOPIC,
            check,
        );
    }
}
