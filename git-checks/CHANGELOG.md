# v4.2.2

  * CI updates to publish from CI.

# v4.2.1

## Changes

  * Update to require `git-checks-config` 0.2.1 for the `config` feature to
    ensure proper static registration.
  * MSRV bumped to 1.62 (when using the `config` feature).

# v4.2.0

## New checks

  * The `FastForward` check is a branch check that checks whether a topic can
    be fast-forward merged into a target branch.
  * The `RejectBiDi` check is a content check that checks whether any Unicode
    bidirectional control characters are present in any added content. See
    [Trojan Codes][trojan-codes] for more details.

[trojan-codes]: https://www.trojansource.codes

## Changes

  * `CommitSubject` now detects variants of "draft" as a prefix to commit
    subjects as work-in-progress commits.
  * `CommitSubject` now detects `amend!` rebase commands.
  * `CommitSubject` can now detect suggestion application commit messages from
    GitLab and GitHub.
  * `RejectBinaries` now detects PE (Windows) binaries.
  * `inventory` 0.2 is now used. Consumers will need to match with it to see
    the instances. There is now a re-export of `inventory` when the `config`
    feature is used.
  * MSRV bumped to 1.47.

# v4.1.0

## Changes

  * `ValidName::whitelisted_domains` is now `trust_domains`. Backwards
    compatibility is provided.

# v4.0.3

## Changes

  * `ValidName::ValidNameFullNamePolicy` now implements `PartialEq` and `Eq`
  * The `SubmoduleWatch` check now warns if an unconfigured submodule is
    modified.

## Fixes

  * The `SubmoduleWatch` check now properly detects submodules which transform
    from or into non-submodules in the same commit.

# v4.0.2

  * Improve logging and collection when a formatter fails in the `Formatting`
    check.

# v4.0.1

  * Update private dependencies.

# v4.0.0

## Breaking changes

  * The `RejectConflictPaths` is now only constructable via its `new`
    constructor method.
  * No more `error-chain`.
  * `ToString` has been replaced with `Into<String>` in APIs. This shouldn't
    affect any code except those using array literals.
    `takes_to_string_iter(&["literal"])` becomes
    `takes_into_string(["literal"].iter().cloned())`.
  * All checks have been converted over to a builder pattern. This carries a
    big API change, but it has the following benefits:
    - Configurations on checks are documented in just one place now (rather
      than the field and associated method).
    - Names appear in configuration calls. For example, instead of an opaque
      `ThirdParty("blah", "blah", "blah", "blah")` call, each now has a name
      associated with the call on the builder.
    - Consistency. All checks now support the builder pattern, but offer an
      `impl Default` if available.
  * There is now a `config` feature which registers deserialization for all
    checks in the crate through the mechanisms provided by `inventory` and
    `git-checks-config`.

## New checks

  * The `BadCommit` check is a single-commit variant of `BadCommits` which
    allows for a reason to be given for the rejection.

## Changes

  * The core functionality has been split into the `git-checks-core` crate.
  * Various checks have gained `Clone` or `Copy` implementations as available.

## Fixes

  * The `RejectConflictPaths` check's `Default` implementation now matches its
    `::new()` method.

## Updated checks

  * The `Changelog` check now supports looking for a set of files for entries.
  * The `CheckExecutablePermissions` check now detects binaries using the same
    logic as the `RejectBinaries` check.
  * The `CheckExecutablePermissions` check now detects AR binaries (used for
    static libraries on ELF platforms and import libraries on Windows).
  * The `CheckExecutablePermissions` now ignore LFS-tracked files.
  * The `Formatting` check now passes the value of the formatting attribute to
    the formatter if set. Previously, a formatting attribute with a value was
    ignored.
  * The `RejectBinaries` check now detects AR binaries.
  * The `RejectConflictPaths` check can now be configured to reject any
    conflict resolution path regardless whether the base file exists or not.
  * The `Changelog` now requires that changelog file contents change. Changing
    the mode on a changelog file is no longer sufficient.
  * The `InvalidPaths` check now supports enforcing Windows filename rules.
  * The `CheckSize` check now has a `Default` implementation. It is 1 MiB by
    default.

# v3.9.1

## Updated checks

  * The `BadCommits` check now has a `TopicCheck` implementation which is
    equivalent, but more efficient, than its `Check` implementation.

# v3.9.0

## Updated checks

  * The `InvalidPaths` check now supports allowing ` ` in paths (ASCII 0x20).

# v3.8.0

## Updated checks

  * The `CommitSubject` check now supports checking for prefixes tolerated
    according to a regular expression.

# v3.7.0

## Updated checks

  * The `CommitSubject` check now supports checking for allowed and disallowed
    prefixes. This does cause the check to no longer `impl Copy`, a technically
    breaking change, but is not deemed worthy enough to require a major version
    bump.

# v3.6.0

## Changes

  * Per-commit check information is now returned in a topological order. That
    is, when iterating using `TopicCheckResult::commit_results`, no commit will
    be listed if its parent hasn't already been listed unless it is already
    part of the target branch.

## Fixes

  * The `SubmoduleRewind` check no longer errors when *updating* a submodule
    reference we can't find.

## New checks

  * The `RejectBinaries` checks that ELF and Mach-O binaries are not added to
    the repository.

# v3.5.3

## Fixes

  * No longer creates git work areas when there are no checks.

# v3.5.2

## Fixes

  * Now works with rust 1.29 which stabilized an `Iterator::flatten`
    function that conflicts with `Itertools::flatten`.

# v3.5.1

## Fixes

  * Now works with `arrayvec` (a dependency of `rayon-1.0.0`) which causes
    coersions to fail. See https://github.com/rust-lang/rust/issues/48406.

# v3.5.0

## Updated checks

  * `ValidName` may now be given domains to assume are valid.

# v3.4.0

## Updated checks

  * `ValidName` now caches its DNS query results for domain names. It is no
    longer `Copy`, but it is still `Clone` (the cache is cleared when cloned).
  * `Formatting` now cleans up child formatting processes which timed out.

# v3.3.0

  * The `Content` trait now can prepare a new workarea for a context using the
    `workarea` method.
  * `CheckGitContext` has deprecated `checkout` and `cd_to_worktree` methods
    as unsafe. They should have been `&mut self` methods.
  * `CheckGitContext` now has `workarea` and `workarea_mut` methods to get the
    underlying `GitWorkArea`.

# v3.2.1

## Updated checks

  * `ThirdParty` now refers to a "utility" rather than a "script" in its error
    messages.

# v3.2.0

  * Exposed a `FileName::new` method so that the filename parsing may be used
    externally.

## Updated checks

  * `Formatting` now supports naming the check differently than the attribute
    used to reformat. This may be used to refer to a formatter using a more
    specific name (such as with a version number).

## New checks

  * The `Changelog` checks that changes modify paths related to changelog
    entries. It supports the single file and directory of entries styles of
    changelog management.

# v3.1.1

  * Submodules which have different names and paths are now properly handled.

## Updated checks

  * `SubmoduleWatch` no longer messages about added modules if the module has
    been configured. This allows configuring the module to resolve the
    messages.

# v3.1.0

## New checks

  * The `LfsPointer` check verifies that paths with the `filter=lfs` attribute
    are valid according to the LFS specification.

# v3.0.0

  * Updated to `git-workarea-3.0.0`.

# v2.3.1

  * `Topic` has been fixed to give a diff against its merge base with the
    target branch rather than the current `HEAD` of the target branch.

# v2.3.0

  * `Commit::file_patch` may be used to get the patch contents of a path.
  * Added the `Topic` structure which represents an entire topic.
  * Both `Commit` and `Topic` implement the new `Content` check for use by
    checks which only care about content rather than Git-level metadata (such
    as topology and commit messages).
  * Added the `TopicCheck` trait for checking entire topics as a whole rather
    than individual commits.
  * Added the `ContentCheck` trait for checks which only care about `Content`.
  * The `impl_prelude` now contains two functions, `commit_prefix` and
    `commit_prefix_str` which create a `String` which mentions the commit for
    commits with an optional custom string.

## Updated checks

  * The following checks have been converted to `ContentChecks`:
    - `CheckEndOfLine`
    - `CheckExecutablePermissions`
    - `CheckSize`
    - `CheckWhitespace`
    - `Formatting`
    - `InvalidPaths`
    - `InvalidUtf8`
    - `RejectSymlinks`
    - `RestrictedPath`
  * `BadCommits` and `CheckExecutablePermissions` now support construction
    with iterators.

# v2.2.1

  * Private dependencies bumped.

# v2.2.0

  * Checks may now add a message with a configured severity level rather than
    calling different methods.

## Updated checks

  * `Formatting` now supports a timeout on formatting operations.

# v2.1.1

  * Dependency specifications for `git-workarea` and `error-chain` have been
    relaxed.

# v2.1.0

## Updated checks

  * `Formatting` now supports adding a message about how the formatting can be
    fixed.

# v2.0.0

  * Updated to `git-workarea-2.0.0`.
  * Checks may now mark that messages are temporary.
  * Checks must now be `Send` in order to support being run in parallel via
    `rayon`.
  * `CheckGitContext::cd_to_work_tree` may be used to run a command within the
    internal workarea.
  * `CheckGitContext::check_attr` now accepts arguments via `AsRef`.
  * `Commit::changed_files` may be used to get a list of changed files
    (symlinks and submodules are skipped).
  * `GitCheckConfiguration` now implements `Debug`.
  * There is now an `impl_prelude` module which contains all of the imports
    necessary to write checks.
  * `Commit::sha1_short` has been removed. Checks now always use the full
    commit hash in their messages.
  * Commit messages no longer have an extra newline at their end.

## Fixed checks

  * `CheckEndOfLine` no longer errors out when adding the missing newline at
    the end of a file.
  * `CheckSize` no longer tries to check submodule sizes (which are undefined
    in Git).
  * `SubmoduleRewind` now checks that a commit exists before checking for
    ancestry on a branch.
  * `ValidName` now collapses errors if the commiter and author are the same
    identity.
  * `ReleaseBranch` now checks that all merge bases are valid for the release
    branch.

## Updated checks

  * The `ValidName` check now supports different full name policies:
    - `Required` (the old behavior)
    - `Preferred` (just warns)
    - `Optional` (no diagnostics)
  * `RestrictedPath` can now warn about paths rather than only erroring.

## New checks

  * The `Formatting` check for checking formatting of files has been added.
    See the documentation for the requirements.

# v1.0.0

  * Initial stable release.
