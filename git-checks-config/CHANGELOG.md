# v0.2.2

  * CI updates to publish from CI.

# v0.2.1

  * Update to `inventory` 0.3. This fixes issues with implementations being
    optimized out.
  * MSRV bumped to 1.62 so that `inventory` 0.3 works.

# v0.2.0

  * Update to `inventory` 0.2. There is now an export to allow consumers to
    agree with the crate.
  * MSRV bumped to 1.47.

# v0.1.0

  * Initial release.
