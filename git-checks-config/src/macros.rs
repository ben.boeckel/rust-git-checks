// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use serde::de::DeserializeOwned;

/// Trait for a deserialization structure of a check.
///
/// This trait should be implemented for any structure which can be deserialized and construct a
/// check.
pub trait IntoCheck: DeserializeOwned {
    /// The check parsed by this configuration.
    type Check;

    /// Create a new instance of the check from the configuration.
    fn into_check(self) -> Self::Check;
}

/// Register configuration structures with `inventory`.
///
/// A single check can implement multiple check traits, so this macro accepts multiple checks and
/// multiple registrations with the same check. Ideally, check names should be unique globally, but
/// nothing enforces this at the registration level.
///
/// ## Example
///
/// ```rust,ignore
/// register_checks! {
///     CheckConfig {
///         "name" => CommitCheckConfig,
///         "name/topic" => TopicCheckConfig,
///     },
///     OtherCheckConfig {
///         "other_check" => BranchCheckConfig,
///     },
/// }
/// ```
#[macro_export]
macro_rules! register_checks {
    { $( $ty:ty { $( $name:expr => $tr:ty, )* }, )* } => {
        $(
            $(
                $crate::inventory::submit! {
                    <$tr>::new($name, $crate::CheckCtor { f: <$tr>::ctor::<$ty> })
                }
            )*
        )*
    };
}
