// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// XXX(rust-1.66)
#![allow(clippy::uninlined_format_args)]

//! Configurations for Git checks
//!
//! When using git checks, it is often useful to store what checks to read within
//! configuration files. This crate allows for checks to also offer support for reading
//! structures from configuration files and turning them into instances of checks. Another point
//! is that adding another check to a crate requires consumers to update to consume that new check.
//! Here, the [`inventory`][inventory] is used to create a global registry that consuming
//! applications can use to automatically discover new checks added to the application from
//! anywhere.
//!
//!## Caveats
//!
//! One downside of this is that there is then one "blessed" serialization for a check.
//! This crate aims to not preclude such uses and as such, checks themselves should generally
//! not implement `Deserialize`. Instead, a separate structure should be created which then
//! creates the check.
//!
//! ## Example
//!
//! ```rust,ignore
//! struct MyCheck {
//!     field1: bool,
//! }
//!
//! impl Check for MyCheck {
//!     // implementation
//! }
//!
//! struct MyCheckConfig {
//!     #[serde(default)]
//!     field1: bool
//! }
//!
//! impl IntoCheck for MyCheckConfig {
//!     type Check = MyCheck;
//!
//!     fn into_check(self) -> Self::Check {
//!         MyCheck {
//!             field1: self.field1,
//!         }
//!     }
//! }
//!
//! register_checks! {
//!     MyCheckConfig {
//!         "name_of_check" => CommitCheckConfig,
//!     }
//! }
//! ```

mod registry;
pub use registry::*;

#[macro_use]
mod macros;

#[cfg(test)]
mod test;

/// Re-export to guarantee finding the registered instances.
pub use inventory;
