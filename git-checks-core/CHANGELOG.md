# v1.2.1

  * Update `rayon` to avoid an unsound `crossbeam` requirement
  * CI updates to publish from CI.

# v1.2.0

  * MSRV bumped to 1.47.

# v1.1.0

  * Although technically a breaking change, checks now need to `impl Debug` to
    make their use from the `git-checks-config` crate more reliable. Adding an
    `impl Debug` for check implementations should be trivial.
  * `SubmoduleContext` now stores its fields as `Cow<'a, str>`. While
    technically an API break in the strictest sense, code should need only
    trivial updates.

# v1.0.1

  * Allow for newer rayon semver-compatible updates.

# v1.0.0

## What's new

  * This crate has now been split out from `git-checks-3.9.1`. This allows code
    which just needs to run checks to depend on a smaller crate.
  * No more `error-chain`.
  * Core traits are now using `Box<dyn Trait>` instead of a forced error type.
  * No more panicking. Errors are now propogated up instead of assuming `git`
    is well-behaved over time.
